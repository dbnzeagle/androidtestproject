package com.example.myapplication

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    var count: Int = 0
    var data: Test = Test(id = 0, name = "sss")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("data", data)
        outState.putInt("count", count)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        count = savedInstanceState.getInt("count")
    }

    private fun initViews() {

        loginButton.setOnClickListener {
            count++
            val start = Intent(this, SecondActivity::class.java)
            startActivityForResult(start, 134)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 134) {
            if (resultCode == Activity.RESULT_OK) {
                val value = data?.getStringExtra("data")
                when (value){
                    "red"->layout.setBackgroundColor(resources.getColor(android.R.color.holo_red_light))
                    "blue"->layout.setBackgroundColor(resources.getColor(android.R.color.holo_blue_light))
                    "yellow"->layout.setBackgroundColor(resources.getColor(android.R.color.holo_orange_dark))
                    "green"->layout.setBackgroundColor(resources.getColor(android.R.color.holo_green_dark))
                }

            }
        }
    }

}
