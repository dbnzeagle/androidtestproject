package com.example.myapplication

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class MyApplication : Application() {

    lateinit var prefs:SharedPreferences

    override fun onCreate() {
        super.onCreate()
        this.prefs = getSharedPreferences("app", Context.MODE_PRIVATE)
    }


}