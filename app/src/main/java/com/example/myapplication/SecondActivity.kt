package com.example.myapplication

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        setListeners()
    }

    fun setListeners() {
        red.setOnClickListener { setColorAndFinish("red") }
        blue.setOnClickListener { setColorAndFinish("blue") }
        yellow.setOnClickListener { setColorAndFinish("yellow") }
        green.setOnClickListener { setColorAndFinish("green")
        Log.i("CLick","Click")
        }
    }

    fun setColorAndFinish(color: String) {
        val data = Intent(this, SecondActivity::class.java)
        data.apply {
            putExtra("data", color)
        }
        setResult(Activity.RESULT_OK, data)
        finish()
    }
}